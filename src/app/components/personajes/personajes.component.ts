import { Component, Input, OnInit } from '@angular/core';
import {AfterViewInit, ViewChild} from '@angular/core';
import { Api, Personaje, classPersonajeID } from './../../models/personaje.model';
import { PersonajesService } from 'src/app/services/personajes.service';
import { Router } from '@angular/router';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {MatSort, Sort, MatSortModule} from '@angular/material/sort';
import {LiveAnnouncer} from '@angular/cdk/a11y';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    HttpClientModule,
    MatFormFieldModule, 
    MatInputModule, 
    MatTableModule, 
    MatSortModule, 
    MatPaginatorModule,
    MatButtonModule, MatDividerModule,MatIconModule
  ]
})

export class PersonajesComponent implements OnInit,AfterViewInit {

  @Input() personaje:Personaje = {
    name: '',
    height: '',
    mass: '',
    hair_color: '',
    skin_color: '',
    eye_color: '',
    birth_year: '',
    gender: '',
    homeworld: '',
    films: [],
    species: [],
    vehicles: [],
    starships: [],
    created: '',
    edited: '',
    url: ''
  };

  displayedColumns: string[] = ['id', 'name', 'image'];
  dataSource!: MatTableDataSource<classPersonajeID>;
  datos: classPersonajeID[] = [];

  sortedData: any[] = [];

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort!: MatSort;
  users: any[] = []

  constructor(private personajesService: PersonajesService, private router: Router, private _liveAnnouncer: LiveAnnouncer) {
    this.sortedData = this.datos.slice();
   }

  sendUrlDetalle(data:any){

    console.warn('1.set personaje ' + data.image)
    this.personajesService.seturl(data.image); 
    this.router.navigateByUrl('/detalle');
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

ngOnInit() {
  
  this.personajesService.getPersonajes().subscribe((data:Api) => {
        
    for (let i = 0; i < 6; i++) {

      const element = data.results[i];
      
      let configPers = new classPersonajeID();
      configPers.id = i;
      configPers.name =  element.name;
      configPers.image =  element.url;
      
      this.datos.push(configPers);
    }
    
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource<classPersonajeID>(this.datos); 
    this.dataSource.paginator = this.paginator;
    
  });

}

SortChange(sort: Sort) {
  console.log('sortState',sort)

  const data = this.datos.slice();
  
  if (!sort.active || sort.direction === '') {
    this.sortedData = data;
    return;
  }

  this.sortedData = data.sort((a: any, b: any) => {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'id':
         return this.compare(a.id, b.id, isAsc);
      case 'name':
         return this.compare(a.name, b.name, isAsc);
      default:
        return 0;
    }
    
  })

  this.dataSource = new MatTableDataSource<classPersonajeID>(this.sortedData);//new MatTableDataSource() Sirve para el Filtro. 
  // this.dataSource.sort = this.sort;
  this.dataSource.paginator = this.paginator;

}

compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

}
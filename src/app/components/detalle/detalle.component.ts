import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { Personaje } from 'src/app/models/personaje.model';
import { PersonajesService } from 'src/app/services/personajes.service';

import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Router } from '@angular/router';


export interface UserData {
  id: string;
  name: string;
  progress: string;
  fruit: string;
}

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css'],
  standalone: true,
  imports: [CommonModule,
            MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, MatButtonModule, MatDividerModule,MatIconModule, MatGridListModule, MatListModule, MatCardModule
  ]
})

export class DetalleComponent implements OnInit,OnDestroy {

  detalle: Personaje = {
    name: '',
    height: '',
    mass: '',
    hair_color: '',
    skin_color: '',
    eye_color: '',
    birth_year: '',
    gender: '',
    homeworld: '',
    films: '',
    species: '',
    vehicles: '',
    starships: '',
    created: '',
    edited: '',
    url: ''
  }

  url:string ='';

  subscription: Subscription = new Subscription;

  myValue: any;

  constructor(public personajesService: PersonajesService, public router: Router) { }

  ngOnInit(){
    //obtengo url desde el servicio
    this.subscription =  this.personajesService.geturl().subscribe((value: any) => this.url = value );

    //manda el parametro url al servicio del detalle
    this.personajesService.getDetallePersonajes(this.url).subscribe(res => {
      this.detalle = res; 
      this.detalle.species = ( this.detalle.species.length===0 )?'Empty':this.detalle.species;
      this.detalle.films = ( this.detalle.films.length===0 )?'Empty':this.detalle.films
      this.detalle.vehicles = ( this.detalle.vehicles.length===0 )?'Empty':this.detalle.vehicles
      this.detalle.starships = ( this.detalle.starships.length===0 )?'Empty':this.detalle.starships
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
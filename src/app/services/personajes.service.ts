import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Personaje, Api } from '../models/personaje.model';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {

  personajes : any = [];

  api : Api = {
    count: 0,
    next: "",
    previous: null,
    results : [] 
  };

  public principalesPersonajes: Personaje[] = [];

  // private urldetalle = new ReplaySubject<string>
  private urldetalle = new BehaviorSubject<string>('');

  constructor( private http: HttpClient ) { }

  seturl(url:string|any){
    // this.urldetalle=url;
    this.urldetalle.next(url);

  }
  geturl(){
     return this.urldetalle.asObservable()
   }

  public getPersonajes(): Observable<Api> {
    return this.http.get<Api>('https://swapi.dev/api/people/')
  }

  public getDetallePersonajes(url:string) : Observable<Personaje> {
   return this.http.get<Personaje>(url)
  }

 /* public getImages(image:string){
    this.http.get<Api>(image) 
    .subscribe((data) => { 
        console.log(data);
      });
  }*/
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleComponent } from './components/detalle/detalle.component';
import { PersonajesComponent } from './components/personajes/personajes.component';

const routes: Routes = [
  {path:'', redirectTo: '/personajes', pathMatch: 'full'},
  {path: 'personajes', component: PersonajesComponent },
  {path: 'detalle', component: DetalleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export interface Personaje {

	"name": string,
	"height"?: string,
	"mass"?: string,
	"hair_color"?: string,
	"skin_color"?: string,
	"eye_color"?: string,
	"birth_year"?: string,
	"gender"?: string,
	"homeworld"?: string,
	"films": string[]| string,
	"species": string[]| string,
	"vehicles": string[]| string,
	"starships": string[]| string,
	"created"?:string,
	"edited"?:string,
	"url": string
}

export interface Api {
	"count": number,
	"next": string,
	"previous"?: null,
	"results": Personaje[]
}

export interface classPersonaje {
	"array": classPersonajeID[]
}
export class classPersonajeID {

	"id": number = 0;
	"name": string = '';
	"image": string = '';
}